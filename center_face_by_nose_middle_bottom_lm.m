% This function centers the face in img using the middle point between
% nostrils as reference.
% Inputs:
%   - img: image with the face uncentered
%   - mask: mask uncentered
%   - lm: landmarks uncentered
% Outputs:
%   - c_img: image centered
%   - c_mask: mask centered
%   - c_lm: landmarks centered
function [c_img, c_mask, c_lm] = center_face_by_nose_middle_bottom_lm(img, mask, lm)

mask(mask == 255) = 1;

% detect nostrils position
nose = rgb2gray(img .* repmat(mask, 1, 1, 3));
th = graythresh(nose);
% nose_bin = ~im2bw(nose, th);

% middle bottom point of nose
nose_point = lm(34,:);
nh = round(nose_point(1));
nw = round(nose_point(2));

% middle of image
[w, h, d] = size(img);
mh = round(h/2); 
mw = round(w/2);

% check if its working ok
% figure, imshow(nose)
% hold on, plot(nh, nw, 'r.')
% plot(mh, mw, 'b.');

% shift image
dh = mh - nh;
dw = mw - nw;
c_img = uint8(imshift(img, dw, dh));
c_mask = imshift(nose, dw, dh);
c_lm(:,1) = lm(:,1) + dh;
c_lm(:,2) = lm(:,2) + dw;

% check if its working ok
% figure, imshow(c_img), hold on
% for i=1:66
%     plot(c_lm(i,1), c_lm(i,2), 'r.')
% end