%##########################################################################
%% ZERO STEP: ALIGN FACES
%##########################################################################

clear, clc, close all;

source_imgs_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\';
masks_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\_masks-corrected\';
landmarks_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\_landmarks-corrected\';
results_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\faces-aligned-by-nose\';
if(~exist(results_folder, 'dir')), mkdir(results_folder); end;

% list all 'extension' files in 'folder_imgs'
imgs = dir([source_imgs_folder '*.jpg']);
n_img_files = numel(imgs);

t=tic;
% start the loop for every image in the list
for nf=1:n_img_files
    
    disp(['Image ' num2str(nf) '/' num2str(n_img_files)])
    
    % % Image file name
    img_name = strrep(imgs(nf).name, '.jpg', '');
    
    % % Load image
    img = imread(fullfile(source_imgs_folder, [img_name '.jpg']));
    
    % % Load mask
    mask = imread(fullfile(masks_folder, [img_name '_mask_N.bmp']));
    
    % % Load landmarks
    lm = load(fullfile(landmarks_folder, [img_name '.lm']));
    
    % % Align face
    % [c_img, c_mask, c_lm] = center_face_by_nose(img, mask, lm);
    [c_img, c_mask, c_lm] = center_face_by_nose_middle_nostrils(img, mask, lm); % V dice que mejor
    % [c_img, c_mask, c_lm] = center_face_by_nose_middle_bottom_lm(img, mask, lm);
    
    % % Save aligned face, mask and landmarks
    if(~exist(fullfile(results_folder, 'img'), 'dir')), mkdir(fullfile(results_folder, 'img')); end;
    if(~exist(fullfile(results_folder, 'mask'), 'dir')), mkdir(fullfile(results_folder, 'mask')); end;
    if(~exist(fullfile(results_folder, 'lm'), 'dir')), mkdir(fullfile(results_folder, 'lm')); end;
    imwrite(c_img, fullfile(results_folder, 'img', [img_name '.bmp']));
    imwrite(c_mask, fullfile(results_folder, 'mask', [img_name '.bmp']));
    save(fullfile(results_folder, 'lm', [img_name '.lm']), 'lm');
    
end
disp('Alignation execution ended.');
toc(t)