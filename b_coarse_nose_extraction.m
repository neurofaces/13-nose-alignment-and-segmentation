%##########################################################################
%% FIRST STEP: COARSE NOSE EXTRACTION
%##########################################################################

clear, clc, close all;

verbose = true;

source_imgs_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\faces-aligned-by-nose\img\';
masks_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\faces-aligned-by-nose\mask\';
landmarks_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\faces-aligned-by-nose\lm\';
results_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\faces-aligned-by-nose\coarse\';
if(~exist(results_folder, 'dir')), mkdir(results_folder); end;

% list all 'extension' files in 'folder_imgs'
imgs = dir([source_imgs_folder '*.bmp']);
n_img_files = numel(imgs);


%% get dimensions of all masks
masks_size = zeros(n_img_files, 4);
for nf=1:n_img_files
    
    disp(['Getting dimensions of image ' num2str(nf) '/' num2str(n_img_files)])
    
    mask_name = strrep(imgs(nf).name, '.jpg', '_mask_N.bmp');
    c_mask = imread(fullfile(masks_folder, mask_name));
    
    props = regionprops(c_mask, 'Area', 'BoundingBox');
    areas = [props.Area];
    masks_size(nf, :) = props(max(areas) == areas).BoundingBox();
    masks_size(nf, 3) = masks_size(nf, 1) + masks_size(nf, 3);
    masks_size(nf, 4) = masks_size(nf, 2) + masks_size(nf, 4);
    
end

% testing
if(verbose)
    f = figure; imshow(c_mask), hold on
    for i=1:size(masks_size,1)
        plot(masks_size(i,1), masks_size(i,2), 'r.') % top left
        plot(masks_size(i,1), masks_size(i,4), 'g.') % top right
        plot(masks_size(i,3), masks_size(i,4), 'b.') % bottom right
        plot(masks_size(i,3), masks_size(i,2), 'w.') % bottom left
    end
end

% get biggest mask
mask_coords = [min(masks_size(:,1)) min(masks_size(:,2)) max(masks_size(:,3)) max(masks_size(:,4))];
mask_size = [round(mask_coords(3)-mask_coords(1)) round(mask_coords(4)-mask_coords(2))];
if(verbose)
    plot(mask_coords(1), mask_coords(2), 'r*') % top left
    plot(mask_coords(1), mask_coords(4), 'g*') % top right
    plot(mask_coords(3), mask_coords(4), 'b*') % bottom right
    plot(mask_coords(3), mask_coords(2), 'w*') % bottom left
end

t=tic;
%% Coarse nose extraction
mean_nose = uint16(zeros([fliplr(mask_size) 3]));
v = VideoWriter('noses','MPEG-4');%'Motion JPEG 2000');%'Uncompressed AVI');%'MPEG-4');
% v.CompressionRatio = 5;
v.FrameRate = 5;
open(v);
for nf=1:n_img_files
    
    disp(['Coarse nose extraction for image ' num2str(nf) '/' num2str(n_img_files)])
    
    % % Image file name
    img_name = strrep(imgs(nf).name, '.bmp', '');
    
    % % Load image
    c_img = imread(fullfile(source_imgs_folder, [img_name '.bmp']));
    
    % % Load mask
    c_mask = logical(imread(fullfile(masks_folder, [img_name '.bmp'])));
    
    % % Cut the nose out
    nose_wo_mask = imcrop(c_img, [mask_coords(1:2) mask_size-1]);
    mask = imcrop(c_mask, [mask_coords(1:2) mask_size-1]);
    nose_w_mask = nose_wo_mask.*repmat(uint8(mask),1,1,3);
    
    % % Create a video with all noses to see diferences among them
    mean_nose(:,:,1) = mean_nose(:,:,1) + uint16(nose_w_mask(:,:,1));
    mean_nose(:,:,2) = mean_nose(:,:,2) + uint16(nose_w_mask(:,:,2));
    mean_nose(:,:,3) = mean_nose(:,:,3) + uint16(nose_w_mask(:,:,3));
    writeVideo(v, nose_w_mask);
    
    % % Save extracted nose
    if(~exist(fullfile(results_folder, 'nose_wo_mask'), 'dir')), mkdir(fullfile(results_folder, 'nose_wo_mask')); end;
    if(~exist(fullfile(results_folder, 'nose_w_mask'), 'dir')), mkdir(fullfile(results_folder, 'nose_w_mask')); end;
    if(~exist(fullfile(results_folder, 'mask'), 'dir')), mkdir(fullfile(results_folder, 'mask')); end;
    imwrite(nose_wo_mask, fullfile(results_folder, 'nose_wo_mask', [img_name '.bmp']));
    imwrite(nose_w_mask, fullfile(results_folder, 'nose_w_mask', [img_name '.bmp']));
    imwrite(mask, fullfile(results_folder, 'mask', [img_name '.bmp']));
    
end
mean_nose = uint8(mean_nose / n_img_files);
imwrite(mean_nose, 'mean_nose.bmp');
close(v);
disp('Execution ended.');
toc(t)