% c_resize_images
new_size = 0.6; %0.25;

folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\faces-aligned-by-nose\coarse\nose_wo_mask\';
% folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask-iris-aligned-middle-min-size\';
folder_results = fullfile(folder_imgs, 'cropped\');
if(~exist(folder_results, 'dir')), mkdir(folder_results); end;
extension = '.bmp';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs '*' extension]);
n_img_filenames = numel(img_filenames);

% start the loop for every image in the list to resize images
hw = waitbar(0, 'Resizing images... 0%');
for nf=1:n_img_filenames
    
    waitbar(nf/n_img_filenames, hw, sprintf('Resizing images... %.2f%%', nf/n_img_filenames*100));
    cprintf('blue', ['Resizing image ' num2str(nf) '/' num2str(n_img_filenames) '\n']);
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    % % Load image
    img = imread(fullfile(folder_imgs, [filename extension]));
    
    % % Crop
    img = imcrop(img, [50 225 310 195]);
    
    % % Resize
	resized_img = imresize(img, new_size);
    
    % % Save image       
    imwrite(resized_img, fullfile(folder_results, strcat(filename, '.bmp')));
    
end
disp('Execution ended.');
close(hw);