% This function centers the face in img using the middle point between
% nostrils as reference.
% Inputs:
%   - img: image with the face uncentered
%   - mask: mask uncentered
%   - lm: landmarks uncentered
% Outputs:
%   - c_img: image centered
%   - c_mask: mask centered
%   - c_lm: landmarks centered
function [c_img, c_mask, c_lm] = center_face_by_nose_middle_nostrils(img, mask, lm)

mask(mask == 255) = 1;

% detect nostrils position
nose = rgb2gray(img .* repmat(mask, 1, 1, 3));
th = graythresh(nose);
nose_bin = ~im2bw(nose, th);
% nose_bin = imopen(nose_bin, strel('disk', 3));

try
    props = regionprops(nose_bin, 'Centroid', 'Area');
    centroids = [props.Centroid];
    centroids = [centroids(1:2:end); centroids(2:2:end)]';
    areas = [props.Area]';
    
    % first sieve (areas)
    ind2keep = [props.Area] > 30 & [props.Area] < 5000;
    centroids = centroids(ind2keep, :);
    areas = areas(ind2keep);
    
    % second sieve (most similar vertical position)
    [csv, icsv] = sort(centroids(:,2), 'ascend'); % vertical position
    csvd = diff(csv);
    th_vprox = 10;
    ncsvd = find(csvd < th_vprox);
    indscv = [ncsvd(1); ncsvd+1];
    inds = icsv(indscv);
    centroids = centroids(inds, :);
    areas = areas(inds, :);
    
    % third sieve (minimum horizontal separation)
    [csh, icsh] = sort(centroids(:,1), 'ascend'); % horizontal position
    cshd = diff(csh);
    th_hprox = 30;
    ncshd = find(cshd > th_hprox);
    indsch = unique([ncshd(1); ncshd+1]);
    inds = icsh(indsch);
    % if there are more than 2 centroids with vertical coord very similar:
    areas = areas(inds);
    centroids = centroids(inds,:);
    
    % final sieve (most similar areas):
    [as, ias] = sort(areas, 'ascend');
    asd = diff(as);
    ind_as = find(asd == min(asd));
    indsa = [ind_as ind_as+1];
    inds = ias(indsa);
    
    % finally keep best centroids
    centroids = centroids(inds, :);
    areas = areas(inds, :);
    
    if(isempty(centroids))
        throw(MException('',''));
    end
catch
    % if no nostrils are detected, ask user to select them
    f = figure; imshow(nose), hold on;
    h1 = impoint;
    h2 = impoint;
    centroids = [getPosition(h1); getPosition(h2);];
    close(f);
end

% middle point between nostrils
nose_point = mean(centroids);
nh = round(nose_point(1));
nw = round(nose_point(2));

% middle of image
[w, h, ~] = size(img);
mh = round(h/2);
mw = round(w/2);

% check if its working ok
% figure
imshowpair(nose_bin, nose)
hold on, plot(nh, nw, 'y.', 'MarkerSize', 10)
plot(mh, mw, 'b.');

% shift image
dh = mh - nh;
dw = mw - nw;
c_img = uint8(imshift(img, dw, dh));
c_mask = imshift(nose, dw, dh);
c_lm(:,1) = lm(:,1) + dh;
c_lm(:,2) = lm(:,2) + dw;

% check if its working ok
% figure, imshow(c_img), hold on
% for i=1:66
%     plot(c_lm(i,1), c_lm(i,2), 'r.')
% end